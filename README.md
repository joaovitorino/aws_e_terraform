
# Sobre
Projeto demonstrativo de criação de infra com uso de Terraform e EC2.



## Objetivos

Criar uma instância t2-micro na AWS utilizando Terraform.

Criar alarmes para monitorar essa instância.

Utilizar boas práticas de segurança para garantir que essa máquina não seja comprometida ou não esteja exposta a acessos indevidos.

Expor a porta 443 com algum site - foi utilizado um site do projeto [one-html-page-challenge](https://github.com/Metroxe/one-html-page-challenge)



## Pre-requistos

 - Par de chaves SSH criados previamente na AWS.
 - Um bucket S3 para armazenar o estado do terraform *
 - Terraform instalado

\* Essa parte pode ser removida mas é boa pratica salvar o estado em um local remoto quando existe uma equipe trabalhando nos mesmos projetos


## Passo a passo

1 - Realize download da chave ssh ja criada na AWS

2 - Faça o clone desse repositorio

3 - Edite o arquivo [variables.tf](variables.tf), principalmente a variavel `ec2_user_key` e `key_name`.

4 - Execute os comandos abaixo para executar o terraform

```
$ terraform init
$ terraform plan # Para verificar se esta tudo certo
$ terraform apply 
```

5 - Para desfazer todas as alterações
```
$ terraform destroy
```

6 - Utilize o valor do Output `DNS_instancia` no browser para testar a aplicação. No exemplo abaixo o endereço seria "`https://ec2-18-209-20-141.compute-1.amazonaws.com`"


![Exemplo](images/exemplo_endereco_de_acesso.png)



## Possiveis melhorias

* Adicionar um certificado (necessário DNS)

* Redirecionar HTTP para HTTPS

* Usar um serviço de DNS como o Router 53

* Configurar envio de alerta por SMS ou email

* Configurar terraform para criar a chave ssh ao invés de usar uma existente
