resource "aws_instance" "srv" {
    ami = "${var.ami}"
    instance_type = "${var.instance_type}" 
    vpc_security_group_ids = [ "${aws_security_group.default.id}" ]
    key_name = "${var.key_name}"
    tags = {
        Name = "ton"
        Env = "dev"        
    }        

  user_data = "${file("install.sh")}"
}

resource "aws_cloudwatch_metric_alarm" "cpu_alarm" {
  alarm_name = "cpu-utilization"
  namespace = "AWS/EC2"
  evaluation_periods = 3
  period = "60"
  metric_name = "CPUUtilizarion"
  threshold = "85"
  statistic = "Average"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  dimensions = {
    "InstanceId" = "aws_instance.srv.id"    
  }  
}

terraform {
  backend "s3" {
    bucket = "terraform-state-ton"
    key    = "terraform.tfstate"
    region = "us-east-1"
  }
}

output "DNS_instancia" {
  value = aws_instance.srv.public_dns
}

 