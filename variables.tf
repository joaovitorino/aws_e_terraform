variable "aws_region" {
    description = "Regiao onde a instancia será executada"
    default = "us-east-1"   
}

variable "ami" {
  description = "Amazon Linux 2"
  default = "ami-0d5eff06f840b45e9"  
}

variable "instance_type" {
    description = "Tipo de instancia"
    default = "t2.micro"  
}

variable "key_name" {
  description = "Nome da chave ssh previamente configurada na ANS"
  default = "amilinux2"
}


variable "ec2_user_key" {
  description = "Caminho da chave SSH para conexao a AWS"
  default = "~/Downloads/amilinux2.pem"
}
